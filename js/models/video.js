define([
	'jQuery',
	'Underscore',
	'Backbone'
], function($, _, Backbone){
	var VideoModel = Backbone.Model.extend({
		defaults: {
			Body: 'Body',
			Brand: 'Brand',
			"Post date": 'Post Date',
			Teaser: 'Teaser',
			Thumbnail: 'Thumbnail',
			"Video Link": 'Video Link',
			title: 'title'
		}

	});
	return VideoModel;
});
