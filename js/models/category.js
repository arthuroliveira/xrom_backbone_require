define(['jQuery','Underscore','Backbone','collections/videos', 'models/video'], function($,_,Backbone, Videos, VideoModel) {
   
	var CategoryModel = Backbone.Model.extend({
		defaults: {
			name: "Category no Name"
		},

		initialize:function(){
			this.set('videos',new Videos());
		},

		add: function(obj) {
			var tempVideo = new VideoModel(obj);
			this.get('videos').add(tempVideo);
		},
	});
	
	return CategoryModel;
});