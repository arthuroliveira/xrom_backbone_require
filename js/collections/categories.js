define([
	'jQuery',
	'Underscore',
	'Backbone',
	'models/category'
], function($, _, Backbone,CategoryModel) {
   
	var CategoriesList = Backbone.Collection.extend({
		model: CategoryModel,
		serviceURL: "http://djoglobal.com/hidden/data/education-videos",
		// serviceURL: "./education-videos.json",
		
		init:function(callback){
			this.update(callback);
		},

		update: function(callback){
			var that = this;
			that.reset();
			console.log('Reset',this.length);
	
			$.ajax({
				url: that.serviceURL,
				dataType: "jsonp",
				jsonpCallback: "jsonp123",

				success: function(data) {
					var tempCategory;
					
					_.each(data.nodes, function(item, index){ 
						if (that.where({name: item.node.Brand}).length == 0) {
							tempCategory = new CategoryModel({name: item.node.Brand});
						}

						tempCategory.add(item.node);


						that.add(tempCategory);
					});

					callback();
				},
				error: function(data) {
					alert('error');
					console.log(data);
				}
			});
		}
	});

	return new CategoriesList; 
});