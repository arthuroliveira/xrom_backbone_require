define([
	'jQuery',
	'Underscore',
	'Backbone',
	'models/video'
], function($, _, Backbone, VideoModel) {
	var Videos = Backbone.Collection.extend({
		model: VideoModel,

		initialize: function(){
		}

	});
	return Videos;
});
