// Filename: router.js
define([
	'jQuery',
	'Underscore',
	'Backbone',
	'collections/categories',
	'views/home/main',
	'views/video/main'
], function($, _, Backbone, categories, mainHomeView, videoView ){
	var AppRouter = Backbone.Router.extend({

		routes: {
			// Define some URL routes
			'home': 'goHome',
			'video/:categoryId/:videoId': 'goVideo',
			// Default
			'*actions': 'goHome'
		},
		goHome: function(actions){
			console.log('goHome');
			$('body').attr('class','page-home');
			mainHomeView.render();
		},
		goVideo: function(categoryId,videoId){
			console.log('goVideo',categoryId,videoId);
			$('body').attr('class','page-video');
			videoView.render(categoryId,videoId);
		}
	});

	var initialize = function(){
		var app_router = new AppRouter;
		categories.init(function(){
			Backbone.history.start();
		});
	};
	return { 
		initialize: initialize
	};
});
