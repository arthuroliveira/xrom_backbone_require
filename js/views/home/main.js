define([
	'jQuery',
	'Underscore',
	'Backbone',
	'iScroll',
	'text!templates/home/main.html',
	'views/home/side-bar',
	'views/home/videos',
	'collections/categories'
], function($, _, Backbone, iScroll, mainHomeTemplate, sideBarView, videosView, categories){
	var MainHomeView = Backbone.View.extend({
		el: $("#page"),
		mainScroller: null,

		initialize: function(){
		},

		render: function(){
			var that = this;
			this.$el.fadeOut(400,function(){
				that.$el.show();
				that.$el.html(mainHomeTemplate);

				videosView.render();
				sideBarView.render();

				that.init();
			});
		},

		init: function(){

			function onResize() {
				wrapperH = window.innerHeight;
				wrapperW = window.innerWidth;

				if (wrapperW >= wrapperH) {
					goLandscape();
				} else if (wrapperW >= 767) {
					goPortrait(false);
				} else {
					goMobile();
				}
			};

			function goLandscape() {
				var body = $('body');
				body.addClass('landscape');
				body.removeClass('portrait');
				body.removeClass('mobile');

				sideBarView.onSideBarToogle = function(){
					$('#main-wrapper').css('height',wrapperH-45);
				};
				
				$('#side-bar-wrapper').removeAttr( 'style' );
				sideBarView.onSideBarToogle();
			};

			function goPortrait() {
				var body = $('body');
				body.addClass('portrait');
				body.removeClass('landscape');
				body.removeClass('mobile');

				sideBarView.onSideBarToogle = function(){
					wrapperH = window.innerHeight;
					wrapperH -=  $('#side-bar-wrapper').height() + parseInt($('#side-bar-wrapper').css('margin-top')) + $('#footer').height();	
					
					$('#main-wrapper').css('height',wrapperH);
				};

				$('#side-bar-wrapper').removeAttr( 'style' );
				sideBarView.onSideBarToogle();
			};

			function goMobile() {
				var body = $('body');
				body.addClass('mobile');
				body.removeClass('portrait');
				body.removeClass('landscape');

				sideBarView.onSideBarToogle = function(){
					wrapperH = window.innerHeight;
					$('#main-wrapper').css('height',wrapperH);
				};

				$('#side-bar-wrapper').removeAttr( 'style' );
				sideBarView.onSideBarToogle();
			}

			window.addEventListener('onorientationchange' in window ? 'orientationchange' : 'resize', onResize, false);

			onResize();
			this.initScrolling();
		},

		initScrolling : function () {

			/**
			 * Method called to fetch new videos
			 */
			 var that = this;

			function pullDownAction () {
				setTimeout(function () {	// <-- Simulate network congestion, remove setTimeout from production!
					// var el, li, i;
					// el = document.getElementById('thelist');

					// for (i=0; i<3; i++) {
					// 	li = document.createElement('li');
					// 	li.innerText = 'Generated row ' + (++generatedCount);
					// 	el.insertBefore(li, el.childNodes[0]);
					// }
					categories.update(function(){
						videosView.render();
					});
					// that.mainScroller.refresh();		// Remember to refresh when contents are loaded (ie: on ajax completion)
				}, 1000);	// <-- Simulate network congestion, remove setTimeout from production!
			}

			pullDownEl  = document.getElementById('pullDown');
			pullDownOffset = pullDownEl.offsetHeight;
			
			this.mainScroller = new iScroll('main-wrapper',{
				desktopCompatibility:true, lockDirection:true, topOffset: pullDownOffset,
				
				onRefresh: function () {
					var pulldown = $('#pullDown');
					if (pulldown.attr('class') == 'loading') {
						pulldown.removeAttr('class')
						$('.pullDownLabel',pulldown).html('Pull down to refresh...');
					}
				},

				onScrollMove: function () {
					var pulldown = $('#pullDown');
					if (this.y > 5 && pulldown.attr('class') != 'flip') {
						pulldown.attr('class','flip')
						$('.pullDownLabel',pulldown).html('Release to refresh...');
						this.minScrollY = 0;
					} else if (this.y < 5 &&  pulldown.attr('class') == 'flip') {
						pulldown.removeAttr('class')
						$('.pullDownLabel',pulldown).html('Pull down to refresh...');
						this.minScrollY = -pullDownOffset;
					} 
				},

				onScrollEnd: function () {
					var pulldown = $('#pullDown');
					if (pulldown.attr('class')=='flip') {
						pulldown.attr('class','loading')
						$('.pullDownLabel',pulldown).html('Loading...');
						pullDownAction();	// Execute custom function (ajax call?)
					} 
				}
			});

			sideBarView.mainScroller = this.mainScroller;
			videosView.mainScroller = this.mainScroller;
			sideBarView.initScrolling();
		}
	});
	return new MainHomeView();
});
