define([
	'jQuery',
	'Underscore',
	'Backbone',
	'iScroll',
	'Touchwipe',
	'text!templates/home/side-bar.html'
], function($, _, Backbone, iScroll, Touchwipe, sideBarTemplate){
	var SideBarView = Backbone.View.extend({
		isExpanded : true,
		mainScroller: null,
		onSideBarToogle: null,
		
		render: function () {
			this.el = $("#side-bar-wrapper");
			
			var data = {
				categories: [],
				brands: [],
				_: _ 
			};

			var compiledTemplate = _.template( sideBarTemplate, data);
			this.el.html(compiledTemplate);
		},

		initScrolling: function(){
			// var slideShow = new iScroll('slideshow-wrapper', {
			// 	snap: true,
			// 	momentum: false,
			// 	hScrollbar: false,
			// 	onScrollEnd: function () {
			// 		document.querySelector('#indicator > li.active').className = '';
			// 		document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
			// 	}
			//  });


			var that = this;
			$('#side-bar-wrapper .toogle-button').click(function(){
				if (that.isExpanded) {
					that.collapse();
				} else {
					that.expand();
				}
			});
			
			// $(".swipe").touchwipe({
			// 	wipeLeft: function() { 
			// 		that.collapse();
			// 	},
			// 	wipeRight: function() {
			// 		that.expand();
			// 	},
			// 	min_move_x: 20,
			// 	min_move_y: 20
			// });
		},

		collapse:function(){
			that = this;
			$('.landscape #side-bar-wrapper').animate({
				"margin-left": "-295px"
			}, 500,function(){
				that.el.removeClass('expand');
				that.el.addClass('collapse');

				that.onSideBarToogle();
				that.mainScroller.refresh();
			});
			$('.landscape #main-wrapper').animate({
				"margin-left": "0px"
			}, 500);

			$('.portrait #side-bar-wrapper').animate({
				// "margin-top": "-300px"
				"margin-top": -(this.el.height()-53)
			}, 500, function(){
				that.el.removeClass('expand');
				that.el.addClass('collapse');

				that.onSideBarToogle();
				that.mainScroller.refresh();
			});
			$('.portrait #main-wrapper').animate({
				"margin-top": "0px"
			}, 500);

			$('.mobile #side-bar-wrapper').animate({
				// "margin-top": "-300px"
				"top": -(this.el.height()-53)
			}, 500, function(){
				that.el.removeClass('expand');
				that.el.addClass('collapse');

				that.onSideBarToogle();
				that.mainScroller.refresh();
			});
			$('.mobile #main-wrapper').animate({
				"top": "0px"
			}, 500);

			this.isExpanded = false;
		},
		
		expand: function (){
			that = this;
			// Landscape
			$('.landscape #side-bar-wrapper').animate({
				"margin-left": "0px"
			}, 500, function(){
				that.el.removeClass('collapse');
				that.el.addClass('expand');

				that.onSideBarToogle();
				that.mainScroller.refresh();
			});
			$('.landscape #main-wrapper').animate({
				"margin-left": "0px"
			}, 500);

			// Portrait
			$('.portrait #side-bar-wrapper').animate({
				"margin-top": "0px"    
			}, 500, function(){
				that.el.removeClass('collapse');
				that.el.addClass('expand');

				that.onSideBarToogle();
				that.mainScroller.refresh();
			});
			$('.portrait #main-wrapper').animate({
				"margin-top": "0px"
			}, 500);

			/**
			 * Hack for mobile slide down 
			 */

			if ($('.mobile').length){
				console.log('found',this)
				that.el.css('height','100%')
				that.el.css('top',-(this.el.height()-53))	
			}


			// Mobile
			$('.mobile #side-bar-wrapper').animate({
				"top": "0px"    
			}, 500, function(){

				that.el.removeClass('collapse');
				that.el.addClass('expand');

				that.onSideBarToogle();
				that.mainScroller.refresh();
			});
			$('.mobile #main-wrapper').animate({
				"top": "0px"
			}, 500);


			this.isExpanded = true;
		}
	});
	return new SideBarView();
});
