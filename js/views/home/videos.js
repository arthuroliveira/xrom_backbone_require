define([
	'jQuery',
	'Underscore',
	'Backbone',
	'iScroll',
	'text!templates/home/videos-list.html',
	'collections/categories',
	'models/category'
], function($, _, Backbone, iScroll, videosListTemplate, categories, Category){
	var VideosView = Backbone.View.extend({
		videosScrollers: [],
		mainScroller: null,

		initialize: function() {
			this.collection = categories;
		},

		render: function() {
			var that = this;
			this.el = $("#content");

			var data = {
				categories: this.collection.models,
				_: _ 
			};

			var compiledTemplate = _.template( videosListTemplate, data );
			this.el.html(compiledTemplate);

			var videosContainer = $('.video-wrapper');
			that.videosScrollers = [];
			
			for (var i = videosContainer.length - 1; i >= 0; i--) {
				var listOfLi = $('li',videosContainer[i]);

				$('.video-scroller',videosContainer[i]).css('width', listOfLi.length * 224);  //$(listOfLi).width()
				
				var tempScroll = new iScroll(videosContainer[i],{
					lockDirection:true, 
					vScroll: false
				});

				that.videosScrollers.push(tempScroll);
			};

			window.videos = that.videosScrollers;

			if(that.mainScroller) {
				// setTimeout(function(){
					that.mainScroller.refresh();	
				// },500);
			}
		},
		
		fetchVideos: function() {
			
			var that = this;
			this.el.hide();

			$.ajax({
				url: "http://djoglobaldev.prod.acquia-sites.com/hidden/data/education-videos",
				dataType: "jsonp",
				jsonpCallback: "jsonp123",

				success: function(data) {
					that.collection.update(data);
					$(that.el).fadeIn();
					that.render();


					// that.mainScroller.refresh();
				},
				error: function(data) {
					console.log('error');
					console.log(data);
				}
			});
		}
	});
	return new VideosView();
});



