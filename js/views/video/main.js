define([
	'jQuery',
	'Underscore',
	'Backbone',
	'text!templates/video/main.html',
	'collections/categories'
], function($, _, Backbone,mainVideoTemplate,categories){
	var VideoView = Backbone.View.extend({
		el: $("#page"),

		render: function(categoryId,videoId){
			var template = _.template( mainVideoTemplate, {
				video : categories.at(categoryId).get('videos').at(videoId)
			});
			this.$el.html(template);
			this.init();
		},

		init: function(){
			var video = $('video');

			function onResize() {
				wrapperH = window.innerHeight;
				wrapperW = window.innerWidth;

				if (wrapperW > wrapperH) {
					goLandscape();
				} else {
					goPortrait();
				}
			}

			function goLandscape() {
				var body = $('body');
				body.addClass('landscape');
				body.removeClass('portrait');
				$(video).attr('height', window.innerHeight+"px");
			};

			function goPortrait() {
				var body = $('body');
				body.addClass('portrait');
				body.removeClass('landscape');
				
				$(video).removeAttr('height');
			};

			window.addEventListener('onorientationchange' in window ? 'orientationchange' : 'resize', onResize, false);
			onResize();
		},
	});

	return new VideoView();
});
